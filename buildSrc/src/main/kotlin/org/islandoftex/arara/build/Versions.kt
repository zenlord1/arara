// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.arara.build

/**
 * The plugin and non-plugin versions used during arara's build.
 */
object Versions {
    // plugin dependencies
    const val detekt = "1.18.1"
    const val dokka = "1.5.31"
    const val kotlin = "1.5.31"
    const val shadow = "7.1.0"
    const val spotless = "5.16.0"
    const val spotlessChangelog = "2.2.0"
    const val versionsPlugin = "0.39.0"

    // non-plugin dependencies
    const val clikt = "3.3.0"
    const val coroutines = "1.4.2"
    const val jna = "5.9.0"
    const val junit = "5.8.1"
    const val kaml = "0.36.0"
    const val klock = "2.2.2"
    const val korlibs = "2.2.1"
    const val kotest = "4.6.3"
    const val kotlinLogging = "2.0.11"
    const val kotlinxSerialization = "1.3.0"
    const val jackson = "2.12.3"
    const val log4j = "2.14.1"
    const val mvel = "2.4.12.Final"
    const val slf4j = "1.7.32"
    const val yamlkt = "0.10.2"
    const val ztExec = "1.12"
}
